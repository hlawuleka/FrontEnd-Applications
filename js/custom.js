//Author : Hlawuleka Maswanganyi

$(document).ready( function() {
	
	//Nav logic
	$(window).resize(function(){
		var windWidth = $(this).width();
	
		if(windWidth >= 920){
			$('.navigation ul li#firstMenuItem').removeClass('navigation-menu-item-disguise');
			$(".navigation ul li.mobile-nav-icon").hide();
			$(".navigation ul li.hidden-nav-item").css("display", "inline-block")
		}
		if(windWidth < 921){
			$('.navigation ul li#firstMenuItem').addClass('navigation-menu-item-disguise');
			$(".navigation ul li.mobile-nav-icon").css("display", "inline-block");
			$(".navigation ul li.hidden-nav-item").css("display", "none");
			$(".navigation ul li.isOpen").css("display", "inline-block");
		}
	});

	var menuLinkClickCounter = 0;
	
	//Show welcome message div
	$(".section .info-boards").eq(1).show();

	$(".navigation ul li:not(.mobile-nav-icon) a").click(function(e){

		var which  = $(this).parent().index()-1;
		var toShow = $(".section .info-boards");

		e.preventDefault();
		$(toShow).hide().eq(which).show();

		$(".navigation ul li").children("i").removeClass("active-nav-item").eq(which).addClass("active-nav-item");

	});

	$(".navigation ul li.mobile-nav-icon a").click(function(e){
		e.preventDefault();
		
		menuLinkClickCounter++

		$(".navigation ul li.hidden-nav-item").toggleClass("isOpen").slideToggle(180).delay(30);

		if(menuLinkClickCounter%2 > 0){
			$('.navigation ul li#firstMenuItem').addClass('navigation-menu-item-disguise');
		}else{
			$('.navigation ul li#firstMenuItem').removeClass('navigation-menu-item-disguise');
		}

	});
});
	