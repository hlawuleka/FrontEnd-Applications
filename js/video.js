//Author : Hlawuleka Maswanganyi

var fullscreenSliderInstance = function(fullScreenId, ytFrameId, options){

		this.fullScreenId 		= fullScreenId;
		this.ytFrameId 			= ytFrameId;
		
		//Set default options
		if(options == null || options == {} || options.length == 0){
			this.options = {
				"scrollPlay"           : true,
				"navIconsAutoScroll"   : false,
				"autoOpenDevider"      : true,
				"showCloseOpenIcon"    : false,
				"nextVideoClickable"   : true,
				"enableScrollerMobile" : true,
				"enableScrollable"     : true,
				//Set either "videoRight" || "videoLeft"
				"displayOrder"		   : 0,
				"videoAutoplay"		   : true
			};
		}else{
			this.options = options;
		}

		window.options = this.options;

		var index = 0;
		initIndex  = index;

		//Youtube API Variables
		var player,
			len,
			click = 0,
			ytdList = new Array();

		//Define functions

		var initTasks = function(){
			//Clear Localstorage data
			localStorage.removeItem(fullScreenId+"-playingVideo");

			//Initially hide right nav span on more video section
			$(fullScreenId+" span.more-videos-nav.fa.fa-angle-right").css('z-index', '-1');

			fullscreenSliderInstance.response = videoObject;
			len = videoObject.length-1;
			
			//Init slider information update
			$(fullScreenId+" #video-overlay h1 span#textContainer").text(videoObject[initIndex].title);
			$(fullScreenId+" #video-overlay p").text(videoObject[initIndex].blurb);
			$(fullScreenId+" #next-vid-text>span#next-vid-text").text(videoObject[initIndex+1].title.replace(/^(.{30}[^\s]*).*/, "$1")+"...");
			$(fullScreenId+" #video-overlay .centered-contet .video-navigation .video-counter").text((initIndex+1 > len ? len : initIndex+1) + " / " + (len+1));

			//Load / add more videos
			for(var i = 0;i<=len;i++){
				var title = videoObject[i].title.replace(/^(.{20}[^\s]*).*/, "$1")+"...";

				//Populate ytdList list array
				ytdList.push(videoObject[i].video_id);
				$(fullScreenId+" .vid .moreVids ul").append("<li id='"+videoObject[i].video_id+"'style=background-image:url(images/"+videoObject[i].image+") >'<span id='live-video'></span><p id='small-vid-overlay'><span class='fa fa-play-circle-o'></span><span id='text'>"+title+"</span><span id='dateHolder'>"+videoObject[index].date+"</span></p><a href='#'><i id='playerThumbNail' class='fa fa-youtube-play'></i><a></li>");
			}

			//Initially store the first video id on local storage
			if(Storage !== "undefined"){
				localStorage.setItem(fullScreenId+"-playingVideo", $(fullScreenId+" .vid .moreVids ul li").eq(index).attr("id"));
			}
		}
		initTasks();

		setCurrentlyPlaying = function(){

			var itemId = localStorage.getItem(fullScreenId+"-playingVideo");
			//Add check before adding 1 to cater for last element in the list - soled 'this.indexOutOfBoundsException'
			try{
				var listItemPosition = fullscreenSliderInstance.response[$("#"+itemId).index()+(ytdList.indexOf(itemId) >= len ? -len : 1)].title.replace(/^(.{30}[^\s]*).*/, "$1")+"...";
			}catch(e){
				
				listItemPosition = fullscreenSliderInstance.response[0].title.replace(/^(.{30}[^\s]*).*/, "$1")+"...";
				console.log("Could not find reference some object feild!");
			}
			finally{

				$(fullScreenId+" .vid .moreVids ul li span#currently-playing").hide();
				$(fullScreenId+" p#small-vid-overlay span:not(#text)").css("color", "#ffffff");
				$(fullScreenId+" .vid .moreVids ul li#"+itemId+" p#small-vid-overlay span.fa.fa-play-circle-o").css("color", "red");
			    $(fullScreenId+" .vid .moreVids ul li#"+itemId+"").append('<span id="currently-playing">playing '+($("#"+itemId).index()+1)+' / '+(len+1)+'</span>');
				$(fullScreenId+" #next-vid-text>span#next-vid-text").attr("class", ytdList[(ytdList.indexOf(itemId)+1)] ).html(listItemPosition);
			}
			//console.log('finished playing '+itemId+' playing... '+listItemPosition);
			return;
		}

		function PlayWhenScrollViewReached(){

			var windowScrollTop = 0, 
				vidArea 		= 0;

			$(window).scroll(function(){

				windowScrollTop = $(this).scrollTop();
				vidArea = $(fullScreenId+"")[0].offsetTop;

			    if(windowScrollTop >= vidArea/3 && windowScrollTop < 1100){
			      player.unmute;
			      player.playVideo();
				}
				if(windowScrollTop > 1200 || windowScrollTop < vidArea){

			      //track unmute click so user does not have to unmate if they chose not to mute
			      	//player.mute();
				    player.pauseVideo();
				}

			});
		}
		
		//Play / Pause
		var playPauseButtonToggleClass = function(){
			$(fullScreenId+" #play").removeClass("fa-pause");
			return;
		}

		//Enable / Disable nav items
		var toggleNavItems = function(whatToDisable, whatToEnable){
			$(whatToDisable).addClass("inavtiveButton");
			$(whatToEnable).removeClass("inavtiveButton").addClass("activeButton");
			return;
		}

		this.onreadyYTDState   = function(event){
	      	//PlayWhenScrollViewReached();
 
			$(fullScreenId+" #play").on("click", function() {

			   	if(window.options.scrollPlay == true){
			   	
			   	//Scroll to where the currently playing video is
			   	var  moreVidsLi 		  =  $(fullScreenId+" .vid .moreVids ul li").first().outerWidth(),
			   		 scrollTOPlayingVideo =  moreVidsLi * $(fullScreenId+" .vid .moreVids ul li").eq(index).index();
				   
				   if(scrollTOPlayingVideo > (moreVidsLi*5)){
				   		$(fullScreenId+" span.more-videos-nav.fa.fa-angle-right").css("z-index", "1");
				   }

				   $(fullScreenId+" .vid .moreVids").animate({
				   		scrollLeft: scrollTOPlayingVideo
				   });

				 }

			   	//set autoplay=1 to mimick 'Play' button click
			    if(localStorage.getItem(fullScreenId+"-playingVideo") !== $(fullScreenId+" .vid .moreVids ul li").eq(index).attr("id")){
				   $(fullScreenId+" .vid iframe").attr("src", 'https://www.youtube.com/embed/'+$(fullScreenId+" .vid .moreVids ul li").eq(index).attr("id")+'?autoplay=1&wmode=transparent&enablejsapi=1');
					   
				}else{
					event.target.playVideo();
				}

				localStorage.setItem(fullScreenId+"-playingVideo", $(fullScreenId+" .vid .moreVids ul li").eq(index).attr("id"));

			    $(fullScreenId+" .vid .moreVids ul li#"+localStorage.getItem(fullScreenId+"-playingVideo")+" a>i").removeClass("fa fa-pause").addClass("fa fa-youtube-play");
			    
			    //Option to auto-open video and shift-overlay div -> only when the overlay is clossed and video is not playing already
			    if(!$(fullScreenId+" span#VideoCloseIcon").hasClass("fa-arrow-circle-o-right") && !$(this).hasClass("inavtiveButton")){
			    	$(fullScreenId+" #video-overlay h1 span#VideoCloseIcon").trigger("click");
				}
				setCurrentlyPlaying();
			});

			 $(fullScreenId+" #pause").on("click", function() {
			     event.target.pauseVideo();
			});
	
	    } 

		this.stateChangeEvents = function(event){
      		switch(event.data){

      			case YT.PlayerState.PLAYING:
      				setCurrentlyPlaying();
      				
			    	var listItemPosition = fullscreenSliderInstance.response[index+1].title.replace(/^(.{30}[^\s]*).*/, "$1")+"...";
			        $(fullScreenId+" #next-vid-text>span#next-vid-text").text(listItemPosition);
			       
		      	break;

		      	// case YT.PlayerState.PAUSED :
		       //  break;

		    	case YT.PlayerState.ENDED :
		    		var vidId = $(fullScreenId+" .vid .moreVids ul li").eq(ytdList.indexOf(localStorage.getItem(fullScreenId+"-playingVideo"))+1).attr("id"); 
			       //Store user autoplay option locally and auto play next video based on option chosen
			       if(localStorage.getItem(fullScreenId+"-YTD_FullScreen_autoplayOn") == 'y'){
			    	   
			    	   $(fullScreenId+" .vid .moreVids ul li#"+vidId).trigger("click");
			    	   $(fullScreenId+" .vid iframe").attr("src", 'https://www.youtube.com/embed/'+vidId+'?autoplay=1&showinfo=1&modestbranding=1&wmode=transparent&enablejsapi=1');
			   	   }

		       break;
	       }

        }

        //Event Handlers
        $(document).ready(function(){

			/*Autoplay option */
			$(fullScreenId+" .vid h1 span#autoplay-option").click(function(){

				$(this).toggleClass("autoplayon");
				$(this).children().toggleClass("fa-toggle-on");

				if(Storage !== "undefined"){
					if($(this).hasClass("autoplayon")){
						localStorage.setItem(fullScreenId+"-YTD_FullScreen_autoplayOn", 'y');
					}else{
						localStorage.setItem(fullScreenId+"-YTD_FullScreen_autoplayOn", 'n');
					}
				}

			});

			//Open / Close icons
			$(fullScreenId+" #video-overlay h1 span#VideoCloseIcon").click(function(){
				
				//Show more videos section
				$(fullScreenId+" .vid .moreVids").toggle('fast');
				
				//Do the rest
				$(this).toggleClass("fa-arrow-circle-o-right");
				$(fullScreenId+" #video-overlay .centered-contet").toggleClass("shift-centered-content");
				$(fullScreenId+" #video-overlay").toggleClass("shift-vid-left");
				$(fullScreenId+" .vid").toggleClass("shift-vid-section");

				// $(fullScreenId+" #video-overlay .centered-contet .video-navigation span.video-nav-buttons, .fullscreen-video-section #video-overlay .centered-contet .video-navigation .video-counter").toggle();
				$(fullScreenId+" #video-overlay h1 span#textContainer").toggleClass("smaller-font-size");
				$(fullScreenId+" #video-overlay .centered-contet").toggleClass("reduced-margin-top");
			});


			//Next video
			$(fullScreenId+" #next-vid-text>span#next-vid-text").click(function(){
				if(window.options.nextVideoClickable == true){

					var targetListItem = $(this).attr("class").toString(),
						whereToScroll  = ytdList.indexOf(targetListItem);

					var ulLiItem = $(fullScreenId+" .vid .moreVids ul li#"+targetListItem+"");
					
					$(ulLiItem).trigger("click");
					this.scrollCounter = $(ulLiItem).outerWidth() * $(ulLiItem).index();

					//Start scrooling when / if video >= n/window, where n = 5-1 by default
					if(whereToScroll >= 4){
						$(fullScreenId+" .vid .moreVids").animate({
							scrollLeft: this.scrollCounter
						});

						//Enable / Show right button navigation icon
						$(fullScreenId+" span.more-videos-nav.fa.fa-angle-right").css("z-index", "1");
			
					}
					if(whereToScroll >= len){

						$(fullScreenId+" span.more-videos-nav.fa.fa-angle-left").css("z-index", "-1");
						$(fullScreenId+" span.more-videos-nav.fa.fa-angle-right").css("z-index", "1");
					}
				}
				
			});

			//More videos click
			$(fullScreenId+" .vid .moreVids ul li").click(function(e){

				click++;
				//Store id in local storate to compare whether clicked video is the same
				var getYTId = $(this).attr("id"),
					getCurrentVideo = localStorage.getItem(fullScreenId+"-playingVideo");

				if(Storage !== "undefined"){
				 	localStorage.setItem(fullScreenId+"-playingVideo", $(this).attr("id"));
				}

				//If video alreay playing, do not restart, but cater for first video as getYTId equals getCurrentVideo initially
			    if(getYTId !== getCurrentVideo || click == 1) { 
					$(fullScreenId+" .vid iframe").attr("src", 'https://www.youtube.com/embed/'+getYTId+'?autoplay=1&wmode=transparent&enablejsapi=1');
					setCurrentlyPlaying();
				}

			});

			//More videos navigation icons
			var scrollCounter = 0, stepValue = 0, itemsPerWindow = 0;
			this.scrollCounter = scrollCounter;
			
			var toScroll         = $(fullScreenId+" .vid .moreVids"),
				listItemWidth    = $(fullScreenId+" .vid .moreVids ul li").outerWidth(), 
				numOfItems 		 = $(fullScreenId+" .vid .moreVids ul li").length,
				maxScroll        = numOfItems * listItemWidth,
				winSize          = $(window).width(),

				//Check window size and update where to stop scrolling accordingly, if 90 set, then assume 3 items and less else 5 items
				stopPercentage   = winSize <= 1000 ? 90 : 73,
				itemsPerWindow 	 = stopPercentage == 90 ? 2 : 5,

				persPectiveItems = listItemWidth * itemsPerWindow,
				minScroll 		 = maxScroll < persPectiveItems ? 0 : persPectiveItems;

			$(fullScreenId+" span.more-videos-nav.fa.fa-angle-left, "+fullScreenId+" span.more-videos-nav.fa.fa-angle-right").click(function(){
				var currenNavitItem  = $(this).attr("id"),
					calcStopPercVal  = parseInt((scrollCounter/maxScroll)*100);

					stepValue += listItemWidth;

				if(currenNavitItem == "leftVidNav"){
					scrollCounter += listItemWidth;

					$(toScroll).animate({scrollLeft:scrollCounter}, 200);

					//Show / Hide nav buttons
					if(scrollCounter >= listItemWidth){
						$(fullScreenId+" span.more-videos-nav.fa.fa-angle-right").css("z-index", "1");
					}

					if((maxScroll - scrollCounter) < minScroll || calcStopPercVal >= stopPercentage){
						$(fullScreenId+" span.more-videos-nav.fa.fa-angle-left").css("z-index", "-1");
					}
					
				}

				if(currenNavitItem == "rightVidNav"){
					
					scrollCounter -= listItemWidth;
					$(toScroll).animate({scrollLeft: scrollCounter}, 200);

					//Show / Hide nav buttons
					if(scrollCounter < listItemWidth){
						$(fullScreenId+" span.more-videos-nav.fa.fa-angle-right").css("z-index", "-1");
					}
					if((maxScroll - scrollCounter) >= minScroll || calcStopPercVal <= stopPercentage){
						$(fullScreenId+" span.more-videos-nav.fa.fa-angle-left").css("z-index", "1");
					}
				}

				//play / pause
				playPauseButtonToggleClass();
				return;
			});

			//Text areas navigation icons
			$(fullScreenId+" #video-overlay .centered-contet .video-navigation span.video-nav-buttons").click(function(i){
				
			   var which = ($(this).attr("id"));	
			   $(fullScreenId+" span.video-nav-buttons").hide();

				var title = "";
				var blurb = "";

				//Prev clicked
				if(which == "prev"){
					
					if(window.options.navIconsAutoScroll == true){
						//Auto scroll more vids
						$(fullScreenId+" span.more-videos-nav.fa.fa-angle-right").trigger("click");
					}

					if(index <= 0 ){
						index = 0;
					}else{
					    --index;
					}

					title = fullscreenSliderInstance.response[index].title;
				    blurb = fullscreenSliderInstance.response[index].blurb;

				}

				//Next Clicked
				if(which == "next"){
					
					if(window.options.navIconsAutoScroll == true){
						//Auto scroll more vids
						$(fullScreenId+" span.more-videos-nav.fa.fa-angle-left").trigger("click");
					}

					if (index >= len ){
					   index = len;
					}
					else{ 
						++index;
					}

					title = fullscreenSliderInstance.response[index].title;
				    blurb = fullscreenSliderInstance.response[index].blurb;
				}

				//Update Content
				$(fullScreenId+" #video-overlay h1 span#textContainer").text(title);
				$(fullScreenId+" #video-overlay p").text(blurb);

				//Reset Nav icons
				$(fullScreenId+" span.video-nav-buttons").show();
				$(fullScreenId+" #video-overlay .centered-contet .video-navigation .video-counter").text((index+1) + " / " + (len+1));

				// play / pause
				playPauseButtonToggleClass();

			});


			//Set autoplay on / off
			if(localStorage.getItem(fullScreenId+"-YTD_FullScreen_autoplayOn")  == 'y'){
				
				$(fullScreenId+" .vid h1 span#autoplay-option").addClass('autoplayon');
				$(fullScreenId+" .vid h1 span#autoplay-option i").addClass('fa-toggle-on');
			}else{

				$(fullScreenId+" .vid h1 span#autoplay-option").removeClass('autoplayon');
				$(fullScreenId+" .vid h1 span#autoplay-option i").addClass('fa-toggle-off');
			}

			//Options
			if(window.options.showCloseOpenIcon && !window.options.autoOpenDevider){
				window.options.autoOpenDevider = true;
			}
			if(window.options.videoAutoplay == false){
				$(fullScreenId+" .vid h1 span#autoplay-option").hide();
			}
			if(window.options.autoOpenDevider == true){
				$(fullScreenId+" #video-overlay h1 span#VideoCloseIcon").trigger("click");
			}
			if(window.options.showCloseOpenIcon && window.options.autoOpenDevider){
				$(".fullscreen-video-section #video-overlay h1 span#VideoCloseIcon").css("display", "none");
			}

			if(window.options.enableScrollerMobile == false){
				var parentHeight, iframeHeight = 100;

				if($(window).width() <= 620){
					parentHeight = 400;
				}else{
					parentHeight = 600;
				}

				$(".fullscreen-video-section").css("height", parentHeight+"px");
				$(".fullscreen-video-section .vid iframe").css("height", iframeHeight+"%");
				$(".fullscreen-video-section .vid .moreVids, .fullscreen-video-section .vid h1, .fullscreen-video-section .vid .more-videos-nav").hide();

			}
			//Incase there's only one video, set next to the first one by default
			$(fullScreenId+" #next-vid-text>span#next-vid-text").attr("class", ytdList[1] || ytdList[0]);

		});
   	   
}

//Create fullscreen slider instance(s)
var instance00 = new fullscreenSliderInstance('#fullscreen-video-section-00', "ytIframe-01", null);

//Store instance object(s) in array
var instances = [instance00];

function onYouTubePlayerAPIReady() {

	//As YoutubeAPI instance gets created, assign each player object to current iframe object
	try{
	 	for(var i = 0;i<instances.length;i++) {
			player = new YT.Player(instances[i].ytFrameId, {
		    	events: {
				    'onReady'       : instances[i].onreadyYTDState,
				    'onStateChange' : instances[i].stateChangeEvents
				}
			});
		}
	}catch(e) {
		return (e);
	}
}

// A function to give each iframe a unique instance, this allows handing player states possible and easy
function iframeLoader() {
	onYouTubePlayerAPIReady();
}